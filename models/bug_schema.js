var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

var utils = require('./utils');

var Project = require('./project');
var User = require('./user');

var BugSchema =	new mongoose.Schema({
	num: Number,
	project: {type: ObjectId, ref: 'Project'},
	created_on: Date,
	created_by: {type: ObjectId, ref: 'User'},
	status: String,
	title: String,
	txt: String,
	comments : [{
		created_on: Date,
		status: String,
		created_by: {type: ObjectId, ref: 'User'},
		txt: String
	}]
});

BugSchema.index({num:1, project:1}, {unique: true});

/*
BugSchema.pre('init', function (next, doc, query) {
	// hack
	console.log('pre init');
	query.populate('project');
	query.populate('created_by');
	next();
});
*/

BugSchema.pre('save', function(next) {
	var self = this;
	var Bug = require('./bug');

	if (this.num === -1) {
		var newnum = 1;
		self.status = 'open';

		Bug.findOne({project: self.project}).select('num').sort('-num').exec(function(err, lastbug) {
			if (err) {
				next(err);
			}

			if (lastbug) {
				newnum = lastbug.num + 1;
			}

			self.num = newnum;
			next();
		});

	} else {
		next();
	}
});

BugSchema.static({
	findWithProjectUrlAndNum: function(projectUrl, num, cb)  {
		var self = this;
		var Project = require('./project');

		Project.findByUrl(projectUrl, function(err, project) {
				if (err) {
					return cb(err);
				}

				if ( ! project) {
					return cb({
						code: 404,
						msg: 'Project not found'
					});
				}

				self.findOne({project: project._id, num: num})
					.populate('created_by', '-auth')
					.populate('comments.created_by', '-auth')
					.populate('project')
					.exec(function(err, bug) {
						if (err) {
							return cb({code: 500, msg: 'Database error', original: err});
						}

						if ( ! bug ) {
							return cb({
								code: 404,
								msg: 'Bug not found'
							});
						}

						return cb(null, bug);
					})
				;
			})
		;
	}
	,
	createWithProjectUrl: function(projectUrl, data, cb) {
		var self = this;

		var Project = require('./project');

		data.num = -1;
		data.created_on = Date.now();

		Project.findByUrl(projectUrl, function(err, project) {
			if (err) {
				return cb(err);
			}

			if ( ! project) {
				return cb({
					code: 404,
					msg: 'Project not found'
				});
			}

			data.project = project._id;

			var bug = new self(data);

			bug.save(function(err, bug) {
				if (err) {
					if (err.code == 11000) {
						return cb({
							code: 409,
							msg: 'Bug number conflict',
							original: err
						});

					} else {
						return cb({code: 500, msg: 'Database error', original: err});
					}
				}

				return cb(null, {bug: bug, project: project});
			});
		});
	}
	,
	findByProject: function(projectId, options, cb) {
		var self = this;

		if (typeof cb === 'undefined') {
			if (typeof options === 'function') {
				cb = options;
				options = {};

			} else {
				throw "callback required";
			}
		}

		options = utils.mergeRecursive(
			{
				onlyOpen: true
			},
			options
		);

		var q = self.find({project: projectId})
			.sort({'created_on': -1})
		;

		if (options.onlyOpen) {
			q.where('status').equals('open');
		}

		q.exec(function(err, bugs) {
			if (err) {
				return cb({
					code: 500,
					msg: 'Database error fetching project bugs',
					original: err
				});
			}

			return cb(null, bugs);
		});
	}
	,
	countByProject: function(projectId, options, cb) {
		var self = this;

		if (typeof cb === 'undefined') {
			if (typeof options === 'function') {
				cb = options;
				options = {};

			} else {
				throw "callback required";
			}
		}

		options = utils.mergeRecursive(
			{
				onlyOpen: true
			},
			options
		);

		var conds = {};
		if (options.onlyOpen) {
			conds.status = 'open';
		}

		self.count(conds, function(err, count) {
			if (err) {
				return cb({
					code: 500,
					msg: 'Database error fetching project bugs',
					original: err
				});
			}

			return cb(null, count);
		});
	}
});

BugSchema.method({
	appendComment: function(data, cb) {
		var self = this;

		data.created_on = Date.now();

		if (data.status) {
			switch (data.status) {
				case 'reopen':
					self.status = 'open';
				break;

				case 'closed':
				case 'wontfix':
					self.status = 'closed';
				break;
			}
		}

		self.comments.push(data);

		this.save(function(err) {
			if (err) {
				return cb({code:500, msg: 'Database error', original: err});
			}

			return cb(null, self);
		});
	}
});

module.exports = BugSchema;
