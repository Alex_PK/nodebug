var mongoose = require('mongoose');
var UserSchema = require('./user_schema');

var User = mongoose.model('User', UserSchema);

module.exports = User;
