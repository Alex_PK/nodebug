var mongoose = require('mongoose');

var BugSchema = require('./bug_schema');

var Bug = mongoose.model(
	'Bug',
	BugSchema
);

module.exports = Bug;
