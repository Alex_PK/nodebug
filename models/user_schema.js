var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
	name: String,
	email: String,
	auth: {
		local: {
			username: {type: String, unique: true},
			password: String
		}
	}
});

UserSchema.method({
	canDo: function(action, target) {
		return 'pippo';
	}
});

module.exports = UserSchema;