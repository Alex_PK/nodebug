var mongoose = require('mongoose');

var ProjectSchema = require('./project_schema');

var Project = mongoose.model(
	'Project',
	ProjectSchema
);

module.exports = Project;
