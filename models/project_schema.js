var mongoose = require('mongoose');
var async = require('async');

var utils = require('./utils');

var User = require('./user');

var ObjectId = mongoose.Schema.Types.ObjectId;

var ProjectSchema = new mongoose.Schema({
	name: {type: String, unique: true},
	url: {type: String, unique: true},
	created_on: Date,
	created_by: {type: ObjectId, ref: 'User'},
	users: [{type: ObjectId, ref: 'User'}]
});

ProjectSchema
	.virtual('bugCount')
	.get(function() {
		return this._bugCount;
	})
	.set(function(value) {
		this._bugCount = value;
	})
;

ProjectSchema.pre('save', function(next) {
	this.url = utils.urlize(this.name);
	next();
});

ProjectSchema.static({
	createCustom: function(data, cb) {
		var self = this;

		data.created_on = Date.now();

		self.create(data, function(err, project) {
			if (err) {
				if (err.code == 11000) {
					return cb({
						code: 409,
						msg: 'Project name conflict',
						original: err
					});

				} else {
					return cb({code: 500, msg: 'Database error', original: err});
				}
			}

			return cb(null, project);
		});
	}
	,
	findByUrl: function(url, cb) {
		var self = this;

		self.findOne({'url': url})
			.populate('created_by', '-auth')
			.exec(function(err, project) {
				if (err) {
					return cb({code: 500, msg: 'Database error fetching project', original: err});
				}

				return cb(null, project);
			})
		;
	}
	,
	findPaged: function(options, cb) {
		var self = this;

		options = utils.mergeRecursive(
			{
				page: 1,
				perPage: 10
			}
			,
			options
		);

		async.parallel(
			[
				function(next) {
					self.count(next);
				}
				,
				function(next) {
					self.find({})
						.sort('name')
						.skip(options.page * options.perPage)
						.limit(options.perPage)
						.exec(next)
					;
				}
			]
			,
			function(err, results) {
				if (err) {
					return cb({
						code: 500,
						msg: 'Database error fetching projects',
						original: err
					});
				}

				var count = results[0];
				var projects = results[1];

				var lastPage = ((options.page + 1)  * options.perPage) >= count;


				return cb(null, {
					projects: projects,
					tot: count,
					page: options.page,
					perPage: options.perPage,
					lastPage: lastPage
				});
			}
		);

	}
});

ProjectSchema.method({
	getBugs: function(cb) {
		var self = this;
		var Bug = require('./bug');

		return Bug.findByProject(self._id, cb);
	}
	,
	getBugsCount: function(options, cb) {
		var self = this;
		var Bug = require('./bug');

		if (typeof cb === 'undefined') {
			if (typeof options === 'function') {
				cb = options;
				options = {};

			} else {
				throw "callback required";
			}
		}

		options = utils.mergeRecursive(
			{
				onlyOpen: true
			},
			options
		);

		return Bug.countByProject(self._id, {onlyOpen: options.onlyOpen}, function(err, count) {
			if (err) {
				cb(err);
			}

			self.bugCount = count;
			return cb(err, self);
		});

	}
});

module.exports = ProjectSchema;
