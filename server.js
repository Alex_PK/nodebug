//setup Dependencies
var connect = require('connect');
var express = require('express');
var io = require('socket.io');
var expressLayouts = require('express-ejs-layouts');
var flash = require('connect-flash');
var passport = require('passport');
var passportLocalStrategy = require('passport-local').Strategy;

var dbUrl = 'mongodb://nodebug:nodebug@localhost/nodebug';
var db = require('mongoose').connect(dbUrl);
var User = require('./models/user');

var port = (process.env.PORT || 9000);

passport.use(new passportLocalStrategy(
	function(username, password, done) {
		User.findOne({'auth.local.username': username, 'auth.local.password': password})
			.select('-auth')
			.exec(function(err, user) {
				if (err) {
					return done(err);
				}

				if (!user) {
					return done(null, false, {message: 'Incorrect username or password'});
				}

				return done(null, user);
			})
		;
	}
));

passport.serializeUser(function(user, done) {
	done(null, user._id);
});

passport.deserializeUser(function(id, done) {
	User.findById(id, function(err, user) {
		done(err, user);
	});
});

//Setup Express
var app = express();

app.configure(function() {
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');

	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(connect.bodyParser());
	app.use(express.methodOverride());
	app.use(express.cookieParser());
	app.use(express.session({
		secret: 'NoDeBug - bug tracking app for node',
		maxAge: 3600000
	}));

	app.use(expressLayouts);
	app.use(flash());

	app.use(passport.initialize());
	app.use(passport.session());

	/*
	app.use(function(req, res, next) {
		if (req.session) {
			res.locals.session = req.session;

			res.locals.user = null;
			if (req.session.user) {
				res.locals.user = req.session.user;
			}
		}
		next();
	});
	*/

	app.use(app.router);

	app.use(require('less-middleware')({ src: __dirname + '/public', force:true }));
	app.use(connect.static(__dirname + '/public'));
});

app.configure('development', function() {
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true}));
});

//setup the errors
/*
app.error(function(err, req, res, next){
	if (err instanceof NotFound) {
		res.render('404', {
			locals: {
				title: '404 - Not Found'
				,description: ''
				,author: ''
			}
			,status: 404
		});
		
	} else {
		res.render('500', {
			locals: {
				title: 'The Server Encountered an Error'
				,description: ''
				,author: ''
				,error: err
			}
			,status: 500
		});
	}
});
*/

app.listen( port );

//Setup Socket.IO
/*
var io = io.listen(app);
io.sockets.on('connection', function(socket){
	console.log('Client Connected');

	socket.on('message', function(data){
		socket.broadcast.emit('app_message',data);
		socket.emit('app_message',data);
	});

	socket.on('disconnect', function(){
		console.log('Client Disconnected.');
	});
});
*/

///////////////////////////////////////////
//				Routes					 //
///////////////////////////////////////////

/////// ADD ALL YOUR ROUTES HERE	/////////

require ('./routes/user')(app);
require ('./routes/project')(app);


app.get('/', function(req,res) {
	return res.redirect('/project');
});

//A Route for Creating a 500 Error (Useful to keep around)
app.get('/500', function(req, res){
	throw new Error('This is a 500 Error');
});

//The 404 Route (ALWAYS Keep this as the last route)
//app.get('/*', function(req, res) {
//	throw new NotFound;
//});

function NotFound(msg){
	this.name = 'NotFound';
	Error.call(this, msg);
	Error.captureStackTrace(this, arguments.callee);
}

console.log('Listening on http://0.0.0.0:' + port );
