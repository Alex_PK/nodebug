
var async = require('async');
var mongoose = require('mongoose');

var Project = require('../models/project');
var Bug = require('../models/bug');
var loggedIn = require('./middleware/logged_in');
var ObjectId = mongoose.Schema.Types.ObjectId;

var maxResultsPerPage = 20;


module.exports = function(app) {
	app.get('/project', loggedIn, function(req, res, next) {
		var page = req.query.page && parseInt(req.query.page, 10) || 0;

		Project.findPaged({page: page, perPage: maxResultsPerPage}, function(err, data) {
			if (err) {
				return res.send(err.msg, err.code);
			}

			async.forEach(
				data.projects,
				function(item, countNext) {
					item.getBugsCount({activeOnly: true}, function(err, data) {
						countNext(err);
					});
				}
				,
				function(err) {
					res.render('project/index', {
						'title': 'Projects',
						projects: data.projects,
						page: data.page,
						lastPage: data.lastPage,
						tot: data.tot,
						breadcrumb: [
							{url: '/project', title: 'Projects'}
						]
					});

				}
			);
			/*
			res.render('project/index', {
				'title': 'Projects',
				projects: data.projects,
				page: data.page,
				lastPage: data.lastPage,
				tot: data.tot,
				breadcrumb: [
					{url: '/', title: 'Home'},
					{url: '/project', title: 'Projects'}
				]
			});
			*/
		});
	});

	app.get('/project/new', loggedIn, function(req, res) {
		res.render('project/new', {
			title: 'New Project',

			back: {url: '/project', title: 'Projects'},
			breadcrumb: [
				{url: '/project', title: 'Projects'},
				{url: '/project/new', title: 'New'}
			]
		});
	});

	app.post('/project', loggedIn, function(req, res, next) {
		Project.createCustom(
			{
				name: req.body.name,
				created_by: req.user._id
			}
			, function(err, project) {
				if (err) {
					return res.send(err.msg, err.code);
				}

				res.redirect('/project/' + project.url);
			}
		);
	});

	app.put('/project', loggedIn, function(req, res, next) {
		var project = Project.findOne({_id: new ObjectId(req.body._id)});

		/*
		Project.create(req.body, function(err) {
			if (err) {
				if (err.code == 11000) {
					res.send('Conflict', 409);

				} else {
					next(err);
				}
				return;
			}
			res.redirect('/');
		});
		*/
	});

	app.get('/project/:url', loggedIn, function(req, res, next) {
		Project.findByUrl(req.params.url, function(err, project) {
			if (err) {
				return res.send(err.msg, err.code);
			}

			project.getBugs(function(err, bugs) {
				if (err) {
					return res.send(err.msg, err.code);
				}

				res.render('project/details', {
					title: project.name,
					project: project,
					bugs: bugs,

					back: {url: '/project', title: 'Projects'},
					breadcrumb: [
						{url: '/project', title: 'Projects'},
						{url: '/project/'+project.url, title: project.name}
					]
				});

			})
		});
	});

	app.get('/project/:url/bug/new', loggedIn, function(req, res, next) {
		Project.findByUrl(req.params.url, function(err, project) {
			if (err) {
				return res.end(err.msg, err.code);
			}

			res.render('project/bug-new', {
				title: 'New Bug',
				project: project,

				back: {url: '/project/'+project.url, title: project.name},
				breadcrumb: [
					{url: '/project', title: 'Projects'},
					{url: '/project/'+project.url, title: project.name},
					{url: '/project/'+project.url+'/bug/new', title: 'New bug'}
				]
			});

		});
	});

	app.post('/project/:url/bug', loggedIn, function(req, res, next) {
		Bug.createWithProjectUrl(
			req.params.url,
			{
				title: req.body.title,
				created_by: req.user._id,
				txt: req.body.txt
			},
			function(err, data) {
				if (err) {
					return res.send(err.msg, err.code);
				}
				res.redirect('/project/' + data.project.url + '/bug/' + data.bug.num);
			}
		);
	});

	app.get('/project/:url/bug/:num', loggedIn, function(req, res, next) {
		Bug.findWithProjectUrlAndNum(req.params.url, req.params.num, function(err, bug) {
			if (err) {
				return res.send(err.msg, err.code);
			}

			res.render('project/bug-details', {
				title: bug.title,
				project: bug.project,
				bug: bug,

				back: {url: '/project/'+bug.project.url, title: bug.project.name},
				breadcrumb: [
					{url: '/project', title: 'Projects'},
					{url: '/project/'+bug.project.url, title: bug.project.name},
					{url: '/project/'+bug.project.url+'/bug/'+bug.num, title: 'Bug #' +bug.num+ ' : ' +bug.title}
				]
			});
		});
	});

	app.post('/project/:url/bug/:num', loggedIn, function(req, res, next) {
		Bug.findWithProjectUrlAndNum(req.params.url, req.params.num, function(err, bug) {
			if (err) {
				return res.send(err.msg, err.code);
			}

			bug.appendComment(
				{
					created_by: req.user._id,
					txt: req.body.txt,
					status: (req.body.status || '')
				}
				,
				function(err, data) {
					if (err) {
						return res.send(err.msg, err.code);
					}
					res.redirect('/project/' + data.project.url + '/bug/' + data.num);
				}
			);
		});
	});

}
