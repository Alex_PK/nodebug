var passport = require('passport');

var User = require('../models/user');
var notLoggedIn = require('./middleware/not_logged_in');

module.exports = function(app) {
	app.get('/user/login', notLoggedIn, function(req, res) {
		res.render('user/login', {
			'title': 'Log in',
			'message': req.flash('login error'),
			breadcrumb: [
				{url: '/', title: 'Home'},
				{url: '/user/login', title: 'Login'}
			]

		});
	});

	app.post('/user/login', passport.authenticate('local', {
		successRedirect: '/',
		failureRedirect: '/user/login',
		failureFlash: true
	}));

	app.get('/user/logout', function(req, res, next) {
		req.session.destroy();
		res.redirect('/');
	});


	app.get('/user/register', notLoggedIn, function(req, res) {
		res.render('user/register', {
			title: 'Register',
			breadcrumb: [
				{url: '/', title: 'Home'},
				{url: '/user/register', title: 'New user'}
			]
		});
	});

	app.post('/user', notLoggedIn, function(req, res, next) {
		User.create(
			{
				name: req.body.name,
				email: req.body.email,
				'auth.local.username': req.body.username,
				'auth.local.password': req.body.password
			},
			function(err) {
				if (err) {
					if (err.code == 11000) {
						res.send('Conflict', 409);

					} else {
						next(err);
					}
					return;
				}
				res.redirect('/');
			}
		);
	});

	app.get('/user', function(req, res, next) {
		if ( ! res.user) {
			return res.redirect('/');
		}

		res.render('user/profile', {
			title: 'User profile',
			user: req.user,
			breadcrumb: [
				{url: '/', title: 'Home'},
				{url: '/user', title: 'User profile: ' +req.user.name}
			]
		});
	});


}
