function notLoggedIn(req, res, next) {
	if (req.user) {
		return res.redirect('/');

	} else {
		next();
	}
}

module.exports = notLoggedIn;
