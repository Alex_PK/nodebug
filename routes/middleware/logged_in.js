var Mongoose = require('mongoose');

function loggedIn(req, res, next) {
	if ( ! req.user) {
		req.session.referer = req.url;
		return res.redirect('/user/login');

	} else {
		// console.log(req.user.canDo());
		next();
	}
}

module.exports = loggedIn;
