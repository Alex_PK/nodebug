function restrictUserToSelf(req, res, next) {
	if ( ! req.user || req.user.username !== req.username) {
		res.send('Unauthorized', 401);

	} else {
		next();
	}
}

module.exports = restrictUserToSelf;
